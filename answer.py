# DO NOT USE THIS FILE. YOU MAY COPY ITS CONTENT INTO ANOTHER SCRIPT.

"""
    There are a few libs imported here, their use is only optional
    you may import other libs as well but you need to provide
    a requirements.txt if you use any third-party libs.
    """

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import re
import urllib.request
import json
import sqlite3

charsComp1 = 'abcdefghijklmnopqrstuvwxyz'
charsComp2 = '1234567890'
charsComp3 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
charsComp4 = '!@#$%^&*()?'

def generate_password(length: int, complexity: int) -> str :
    
#    print (length)
#    print (complexity)
    
    if complexity == 1:
        password = ''
        for c in range(length):
            password = password + random.choice(charsComp1)
        return password
    elif complexity == 2:
        password = ''
        for c in range(length):
            password = password + random.choice(charsComp1 + charsComp2)
        return password
    elif complexity == 3:
        password = ''
        for c in range(length):
            password = password + random.choice(charsComp1 + charsComp2 + charsComp3)
        return password
    elif complexity == 4:
        password = ''
        for c in range(length):
            password = password + random.choice(charsComp1 + charsComp2 + charsComp3 + charsComp4)
        return password

#    """Generate a random password with given length and complexity
#
#        Complexity levels:
#        Complexity == 1: return a password with only lowercase chars
#        Complexity ==  2: Previous level plus at least 1 digit
#        Complexity ==  3: Previous levels plus at least 1 uppercase char
#        Complexity ==  4: Previous levels plus at least 1 punctuation char
#
#        :param length: number of characters
#        :param complexity: complexity level
#        :returns: generated password
#        """

#
def check_password_level(password: str) -> int:
    lowerAvailable = 0
    upperAvailable = 0
    numberAvailabel = 0
    specialAvailabel = 0
    
    if re.search("[a-z]",password):
        lowerAvailable = 1
    if re.search("[0-9]",password):
        numberAvailabel = 1
    if re.search("[A-Z]",password):
        upperAvailable = 1
    if re.search("[$#@]",password):
        specialAvailabel = 1

    if specialAvailabel == 1:
        return 4
    elif upperAvailable == 1:
        return 3
    elif numberAvailabel == 1:
        return 2
    elif lowerAvailable == 1:
        return 1

#    """Return the password complexity level for a given password
#
#        Complexity levels:
#        Return complexity 1: If password has only lowercase chars
#        Return complexity 2: Previous level condition and at least 1 digit
#        Return complexity 3: Previous levels condition and at least 1 uppercase char
#        Return complexity 4: Previous levels condition and at least 1 punctuation
#
#        Complexity level exceptions (override previous results):
#        Return complexity 2: password has length >= 8 chars and only lowercase chars
#        Return complexity 3: password has length >= 8 chars and only lowercase and digits
#
#        :param password: password
#        :returns: complexity level
#        """
#    pass
#
#
def create_user(db_path: str) -> None:

    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    for x in range(0,10):
        
        print("\n\nUser %s" % (x+1))
        
        url = "https://randomuser.me/api/"
        data = ''
        with urllib.request.urlopen(url) as f:
            data = f.read().decode('utf-8')

        response = json.loads(data)
        users = response['results']

        for user in users:
            nameDict = user['name']
            name = nameDict['title'] + ' ' + nameDict['first'] + ' ' + nameDict['last']
            email = user['email']
            print("User Full Name: %s" % (name))
            print("User Email: %s" % (email))

            passLength = random.randint(6,12)
            passComplexity = random.randint(1,4)

            password = generate_password(passLength, passComplexity)
            print ("Generated Password: %s" % (password))
            passtype = check_password_level(password)
            print ("Password Complexity: %s" % (passtype))

            sqlInsert = "INSERT INTO user(full_name, email, password) VALUES(?, ?, ?)"
            args = (name, email, password)
            c.execute(sqlInsert, args)

        conn.commit()

    conn.close()
    
#    you may want to use: http://docs.python-requests.org/en/master/
#    """Retrieve a random user from https://randomuser.me/api/
#        and persist the user (full name and email) into the given SQLite db
#
#        :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
#        :return: None
#        
#    pass
    

create_user('user_database.db')
